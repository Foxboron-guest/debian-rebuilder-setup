from debian.deb822 import Deb822
from flask import g, request, Flask
from flask_htpasswd import HtPasswdAuth
from os import mkdir
from os.path import join
from sqlite3 import connect
from time import time

app = Flask(__name__)
app.config['FLASK_HTPASSWD_PATH'] = '/etc/nginx/htpasswd'

htpasswd = HtPasswdAuth(app)

DIR = '/var/builds/'
DATABASE = '/var/builds/rebuilder.db'


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = connect(DATABASE)
    return db


def enforce_sane_dirent_name(name):
    if '/' in name or name.startswith('.'):
        raise Exception('Illegal dirent name')


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


@app.route('/new_build', methods=['POST'])
@htpasswd.required
def new_build(user=None):
    metadata = request.files['metadata']
    buildinfo = request.files['buildinfo']
    source = None
    version = None
    for paragraph in Deb822.iter_paragraphs(buildinfo):
        for item in paragraph.items():
            if item[0] == 'Source':
                source = item[1]
            if item[0] == 'Version':
                version = item[1]
    buildinfo.seek(0)

    folder_name = '%s-%s' % (source, version)
    enforce_sane_dirent_name(folder_name)

    directory = join(DIR, folder_name)
    mkdir(directory)

    enforce_sane_dirent_name(metadata.filename)
    enforce_sane_dirent_name(buildinfo.filename)

    timestamp = time()
    metadata.save(join(directory, metadata.filename))
    buildinfo.save(join(directory, buildinfo.filename))
    db = get_db()
    c = db.cursor()

    c.execute('INSERT INTO BUILDS VALUES (?, ?, ?, ?, ?)',
              (source, version, timestamp,
               metadata.filename, buildinfo.filename)
              )

    db.commit()
    return 'OK'
